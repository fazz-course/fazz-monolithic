const express = require('express')
const compression = require('compression')
const app = express()

const routers = require('./src/routers')
const db = require('./src/config/db')
const server = require('./src/config/server')

app.use(compression())
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

app.use('/v1', routers)

server({ app, port: process.env.PORT }, async (servers) => {
    try {
        await db.authenticate({ retry: 2 })
        await db.sync({ alter: true })
        servers.Listening()
    } catch (error) {
        console.log(error)
    }
})
