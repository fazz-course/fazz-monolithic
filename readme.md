<table align="center"><tr><td align="center" width="9999">
<h2 align="center">Express With Monolithic Architecture</h2>
<img src="https://res.cloudinary.com/antikey/image/upload/v1667849615/logofazz_ojpnt2.jpg" align="center" width="150" alt="Project icon">

<p align="center">
    <a href="https://www.fazztrack.com/" target="blank">Our Website</a>
    ·
    <a href="https://www.fazztrack.com/class/backend-golang">Join With Us</a>
    ·
</p>

this repository one of part in fazztrack course, see full course <a href="https://www.fazztrack.com/" target="blank">here..</a>

</td></tr></table>

## 🛠️ Installation Steps

1. Clone the repository

```bash
git clone https://gitlab.com/fazz-course/fazz-monolithic.git
```

2. Install dependencies

```bash
yarn install
```

3. Run the app

```bash
yarn run start
```

🌟 You are all set!

## 💻 Built with

-   [Nodejs](https://nodejs.org/en/)
-   [Expressjs](https://expressjs.com/): for handle http method
-   [Postgres](https://www.postgresql.org/): for DBMS

<div align="center" align="center" width="9999">
<tr>
<p align="center">
Developed with ❤️ in Indonesia 	🇮🇩
</p>
</div>
