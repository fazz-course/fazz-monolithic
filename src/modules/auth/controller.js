const ctrl = {}
const model = require('../users/model')

ctrl.GetUsers = async (req, res) => {
    try {
        const data = model.getUsers()
        return res.status(200).json(data)
    } catch (error) {
        res.status(500).send(error)
    }
}
ctrl.Adduser = async (req, res) => {
    try {
        const data = model.Create(req.body)
        return res.status(200).json(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

module.exports = ctrl
