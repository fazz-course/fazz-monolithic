const express = require('express')
const Router = express.Router()
const ctrl = require('./controller')

Router.post('/', ctrl.Adduser)

module.exports = Router
