const express = require('express')
const Router = express.Router()
const ctrl = require('./controller')

Router.get('/', ctrl.GetUsers)
Router.post('/', ctrl.Adduser)

module.exports = Router
