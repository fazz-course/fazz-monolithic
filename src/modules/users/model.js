const db = require('../../database')
const models = {}

models.getUsers = async () => {
    try {
        const data = await db.Users.findAll({
            include: [
                {
                    model: db.UserDetails,
                    as: 'userDetail'
                }
            ],
            attributes: { exclude: ['password'] }
        })
        return data
    } catch (error) {
        throw error
    }
}

models.Create = async ({ username, email, password, fullname }) => {
    try {
        const data = await db.Users.create(
            {
                username,
                password,
                userDetail: {
                    email,
                    fullname
                }
            },
            {
                include: {
                    model: db.UserDetails,
                    as: 'userDetail'
                }
            }
        )
        return data
    } catch (error) {
        throw error
    }
}

module.exports = models
