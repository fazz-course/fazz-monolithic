const ctrl = {}
const model = require('./model')
const hash = require('../../libs/hash')

ctrl.GetUsers = async (req, res) => {
    try {
        const data = await model.getUsers()
        return res.status(200).json(data)
    } catch (error) {
        res.status(500).send(error)
    }
}
ctrl.Adduser = async (req, res) => {
    try {
        req.body.password = await hash.securePassword(req.body.password)
        const data = await model.Create(req.body)
        return res.status(200).json(data)
    } catch (error) {
        console.log(error)
        res.status(500).send(error)
    }
}

module.exports = ctrl
