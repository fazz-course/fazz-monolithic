const express = require('express')
const Router = express.Router()
const ctrl = require('./controller')

Router.get('/', ctrl.GetProduct)
Router.post('/', ctrl.AddProduct)

module.exports = Router
