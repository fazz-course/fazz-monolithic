const ctrl = {}
const model = require('./model')

ctrl.GetProduct = async (req, res) => {
    try {
        const data = await model.getProducts()
        return res.status(200).json(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

ctrl.AddProduct = async (req, res) => {
    try {
        const data = await model.Create(req.body)
        return res.status(200).json(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

module.exports = ctrl
