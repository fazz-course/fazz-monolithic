const db = require('../../database')
const models = {}

models.getProducts = async () => {
    try {
        const data = await db.Products.findAll()
        return data
    } catch (error) {
        throw error
    }
}

models.Create = async ({ name, price, qty, img }) => {
    try {
        const data = await db.Products.create({
            name: name,
            price: price,
            quantity: qty,
            image: img
        })
        return data
    } catch (error) {
        throw error
    }
}

module.exports = models
