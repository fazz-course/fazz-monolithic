const bcrypt = require('bcrypt')
const hash = {}

hash.securePassword = async (pass) => {
    try {
        const salt = await bcrypt.genSalt(10)
        const password = await bcrypt.hash(pass, salt)
        return password
    } catch (error) {
        throw error
    }
}

module.exports = hash
