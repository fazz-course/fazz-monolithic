const Users = require('./users')
const UserDetails = require('./userDetails')
const Products = require('./products')

module.exports = { Users, UserDetails, Products }
