const db = require('../config/db')
const { DataTypes, Model } = require('sequelize')
class UsersDetail extends Model {}

UsersDetail.init(
    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        userId: {
            type: DataTypes.INTEGER,
            references: { model: 'Users', key: 'id' },
            onDelete: 'CASCADE'
        },
        email: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        fullname: {
            type: DataTypes.STRING(100),
            allowNull: false
        }
    },
    { sequelize: db, timestamps: true, modelName: 'userDetails' }
)

module.exports = UsersDetail
