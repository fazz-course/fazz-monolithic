const http = require('http')

module.exports = ({ app, port }, cb) => {
    const server = {}
    const httpServer = http.createServer(app)

    server.Listening = () => {
        httpServer.listen(port)
        httpServer.on('listening', server.onListening)
    }

    server.onListening = () => {
        let bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + port
        console.log('Listening on ' + bind)
    }

    cb(server)
}
