const express = require('express')
const Routers = express.Router()

const products = require('../modules/products/router')
const users = require('../modules/users/router')

Routers.use('/products', products)
Routers.use('/users', users)

module.exports = Routers
